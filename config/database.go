package config

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Db *gorm.DB

func DbConnect() {

	var err error

	Db, err = gorm.Open(mysql.Open(App.Database.Username+":"+App.Database.Password+
		"@tcp("+App.Database.Host+":"+App.Database.Port+")/"+
		App.Database.Name+"?charset=utf8&parseTime=True"), &gorm.Config{})

	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Unable to load database: %v \n", err))
	}

}

func DbPaginate(vPage int, vLimit int, vSort string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {

		var page int
		if page = 1; vPage > 0 {
			page = vPage
		}

		var limit int
		if limit = 100; vLimit > 0 {
			limit = vLimit
		}

		sort := "created_at ASC"
		if &vSort != nil && len(vSort) > 0 {
			sort = vSort
		}

		pagination := (page - 1) * limit
		return db.Offset(pagination).Limit(limit).Order(sort)
	}
}
