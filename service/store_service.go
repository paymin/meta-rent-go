package service

import (
	"meta-rent/config"
	"meta-rent/model"
	"meta-rent/request"
)

func StoreListService(param *request.List) (resp interface{}, total int64, err error) {

	where := "id IS NOT NULL"
	if param.UserId != nil {
		where += " AND stores.user_id = " + *param.UserId
	}

	var data []model.Store
	db := config.Db
	db = db.
		Model(&data).
		Where(where).
		Count(&total).
		Scopes(config.DbPaginate(param.Page, param.Limit, param.Sort)).
		Find(&data)

	return data, total, nil
}

func StoreGetService(id int) (data model.Store, err error) {

	db := config.Db
	db.Joins("User").
		First(&data, id)

	return data, nil
}

func StoreAddService(param *model.Store) (data interface{}, err error) {

	db := config.Db

	store := param

	row := new(model.Store)
	res := db.Create(&store).Scan(&row)
	if res.Error != nil {
		return nil, res.Error
	}

	return store, nil

}

func StoreUpdateService(param *model.Store) (data interface{}, err error) {

	db := config.Db

	var store model.Store
	db.First(&store, param.Id)
	res := db.Model(&store).Updates(param)

	if res.Error != nil {
		return nil, err
	}

	return config.FormatResUpdate(res.RowsAffected), nil
}

func StoreDeleteService(id int) (data interface{}, err error) {

	db := config.Db
	res := db.Delete(&model.Store{}, id)

	if res.Error != nil {
		return nil, res.Error
	}

	return config.FormatResDelete(res.RowsAffected), nil
}
