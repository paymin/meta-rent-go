package controller

import (
	"github.com/labstack/echo"
	errors "golang.org/x/xerrors"
	"meta-rent/config"
	"meta-rent/middleware"
	"meta-rent/model"
	"meta-rent/request"
	"meta-rent/service"
	"strconv"
)

func ProductCategoryList(c echo.Context) error {

	param := new(request.List)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, errors.New("Binding data error"), nil)
	}

	data, total, err := service.ProductCategoryListService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	resp := config.PageInfo(data, param.Page, param.Limit, total)

	return config.Respon(c, false, nil, resp)

}

func ProductCategoryGet(c echo.Context) error {

	id, err := strconv.Atoi(c.Param("id"))
	data, err := service.ProductCategoryGetService(id)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func ProductCategoryAdd(c echo.Context) error {

	param := new(model.ProductCategory)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, errors.New("Binding data error"), nil)
	}

	if err := c.Validate(param); err != nil {
		return config.Respon(c, true, err, nil)
	}

	e := config.Enfocer
	res, _ := e.HasRoleForUser(strconv.Itoa(middleware.AuthUser.Id), "store_owner")
	if res {
		param.UserId = middleware.AuthUser.Id
	}

	data, err := service.ProductCategoryAddService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func ProductCategoryUpdate(c echo.Context) error {

	param := new(model.ProductCategory)
	if err := c.Bind(param); err != nil {
		return config.Respon(c, true, err, nil)
	}

	data, err := service.ProductCategoryUpdateService(param)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}

func ProductCategoryDelete(c echo.Context) error {

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	data, err := service.ProductCategoryDeleteService(id)
	if err != nil {
		return config.Respon(c, true, err, nil)
	}

	return config.Respon(c, false, nil, data)

}
