package middleware

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"meta-rent/config"
	"meta-rent/model"
	"strconv"
)

var AuthUser model.User

func Auth(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(jwt.MapClaims)
		userId := claims["id"].(string)
		e := config.Enfocer
		roles, _ := e.GetRolesForUser(userId)

		id, _ := strconv.Atoi(userId)
		AuthUser = model.User{
			Id:        id,
			Role:      &roles,
		}

		err := next(c)
		return err
	}
}
