package request

type List struct {
	Id     *string `json:"id"`
	UserId *string `json:"user_id"`
	Sort   string  `json:"sort"`
	Limit  int     `json:"limit"`
	Page   int     `json:"page"`
}