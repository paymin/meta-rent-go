package model

import (
	"gorm.io/gorm"
	"time"
)

type ProductCategory struct {
	Id           int            `json:"id"`
	UserId       int            `json:"user_id"`
	StoreId      int            `json:"store_id"`
	Name         string         `json:"name"`
	Descriptions *string        `json:"descriptions"`
	IsActive     *int           `json:"is_active"`
	CreatedAt    time.Time      `json:"created_at"`
	CreatedBy    int            `json:"created_by"`
	UpdatedAt    time.Time      `json:"updated_at"`
	UpdatedBy    int            `json:"updated_by"`
	DeletedAt    gorm.DeletedAt `json:"deleted_at"`
	Store        *Store         `json:"store" gorm:"foreignKey:CategoryId"`
}

type Product struct {
	Id           int              `json:"id"`
	StoreId      int              `json:"store_id"`
	CategoryId   int              `json:"category_id"`
	Code         string           `json:"code"`
	Name         string           `json:"name"`
	Descriptions *string          `json:"descriptions"`
	Image        *string          `json:"image"`
	Weight       *string          `json:"weight"`
	IsActive     *int             `json:"is_active"`
	CreatedAt    time.Time        `json:"created_at"`
	CreatedBy    int              `json:"created_by"`
	UpdatedAt    time.Time        `json:"updated_at"`
	UpdatedBy    int              `json:"updated_by"`
	DeletedAt    gorm.DeletedAt   `json:"deleted_at"`
	Store        *Store           `json:"store"`
	Category     *ProductCategory `json:"category" gorm:"foreignKey:CategoryId"`
}

type ProductStock struct {
	Id           int            `json:"id"`
	ProductId    int            `json:"store_id"`
	Code         string         `json:"code"`
	Name         string         `json:"name"`
	IsActive     *int           `json:"is_active"`
	CreatedAt    time.Time      `json:"created_at"`
	CreatedBy    int            `json:"created_by"`
	UpdatedAt    time.Time      `json:"updated_at"`
	UpdatedBy    int            `json:"updated_by"`
	DeletedAt    gorm.DeletedAt `json:"deleted_at"`
	Store        *Store         `json:"store"`
	Product      *Product       `json:"product" gorm:"foreignKey:CategoryId"`
}
